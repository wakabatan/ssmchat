=head1 NAME

  pl/site.pl

=head1 DESCRIPTION

  Perl module to keep compatible with RocketBeachized cgi scripts.

=cut

use Suika::CGI;

%main::OPT = %Suika::CGI::param;
%SITE::COOKIE = %Suika::CGI::cookie;

sub t3::printh {
  require Jcode;
  my ($s, $noheader) = @_;
  print STDOUT "Content-Style-Type: text/css\n";
  print STDOUT "Content-Script-Type: application/x-javascript\n";
  print STDOUT "Content-Type: text/html; charset=iso-2022-jp\n\n";
  print STDOUT Jcode->new ($t3::hhead, 'euc')->jis unless $noheader;
  print STDOUT Jcode->new ($s, 'euc')->jis;
  print STDOUT Jcode->new ($t3::foot, 'euc')->jis;
}

sub SITE::get_time {
  Suika::CGI::timetext;
}

sub SITE::error(@) {
  die Jcode->new (join ' ', @_, Carp::longmess ())->utf8;
}

sub SITE::count($) {
   my $w_what = shift || 'visit';
   my $countfile = '/home/wakaba/public_html/-temp/pl/count.dat';
   
   my (@number,$line,$da,$num,@new,$flg,$dmy); $flg=0;
   open(DATA,$countfile) or return "Counter open error: $!";
     @number = <DATA>;
   close(DATA);
   
   @new = ();
   foreach $line (@number) {
     ($da,$num,$dmy) = split(/<>/,$line);
     if ($w_what eq $da) {
       $num++;
       push(@new,$w_what.'<>'.$num."<>d\n");
       $flg = $num;
     } else {
       push(@new,$line);
     }
   }
   
   if ($flg == 0) {
     $flg = 1;
     push(@new,$w_what.'<>'.$flg."<>d\n");
   }
   open(DATA,">$countfile");
     print DATA @new;
   close(DATA);
   $flg;
}

sub SITE::GetCookie {}

sub SITE::rfc822time ($$) {
  my $time = shift;
  my $timezone = shift; my $tz;
  if ($timezone =~ /([+-])([0-9]{2})([0-9]{2})/) {$tz="${1}1"*($2*60+$3)*60}
  
  my ($sec,$min,$hour,$day,$mon,$year) = gmtime($time+$tz);
  $mon = (qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec))[$mon];
  $year += 1900 if $year < 1900;
  #$timezone = sprintf("%+05d", $timezone);
  for ($day,$hour,$min,$sec) {$_ = sprintf("%02d", $_)}
  "$day $mon $year $hour:$min:$sec $timezone";
}

=head1 AUTHOR

Wakaba <w@suika.fam.cx>.

=head1 LICENSE

Public Domain.

=cut

1;
