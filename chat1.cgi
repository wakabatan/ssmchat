#!/usr/bin/perl

=head1 NAME

The mususu chat system -- chat1.cgi

=cut

package chat;
use Path::Tiny;
use lib path (__FILE__)->parent->child ('lib')->stringify;
use CGI::Carp qw(fatalsToBrowser);
use Chat::Util;

my $RootPath = path (__FILE__)->parent;

my $FooterMin = q{
<script src="https://manakai.github.io/js/global.js" async></script>
};
my $FooterA0 = q{
<sw-ads ugc></sw-ads>
<script src="https://manakai.github.io/js/global.js" async></script>
};

our $version = q[ssmchat];
$SITE::about = $version." by \xBC\xE3\xCD\xD5.";
$SITE::about_html = <<EOH;
<address class="author" lang="en">
<a href="https://bitbucket.org/wakabatan/ssmchat">$version</a> by <a href="mailto:wakaba\@suikawiki.org" lang="ja">\xBC\xE3\xCD\xD5</a>.
</address>
EOH
$uri = $main::ENV{SCRIPT_NAME};
$uri =~ s/\.cgi$//;

## -- Environment
   $SITE::default_css = './chat.css';	## style sheet
   $SITE::basepath = '';	## site admin path
   require 'site.pl';	## site admin pl
   require './option.pl';	## options
   require './chat.pl';	## chat system

## -- Mode
package main;
use Chat::Util;

if ($OPT{mode} eq 'msg') {
    if ($OPT{comment} eq 'expire') {&chat::longlog::expire(force=>1)}
    #elsif ($OPT{comment} eq "\xC7\xBE\xA4\xDF\xA4\xBD") {&chat::munou::sendbrain;}
    elsif ($OPT{comment}) {
      $OPT{name} ||= $chat::member::noname;
      $OPT{comment} = htescape $OPT{comment};
      &chat::msglog::write($OPT{name},$OPT{comment}.$ENV{foot},$OPT{mail},
                           $OPT{munou},$ENV{REMOTE_ADDR},$OPT{color},$OPT{msgcolor});
    }
  &chat::msglog::show;
  print STDOUT $FooterMin;
} elsif ($OPT{mode} eq 'login') {
    $OPT{name} ||= $chat::member::noname;
    &chat::msglog::write($chat::member::master::name,(htescape $OPT{name}).$chat::member::master::welcome,'','',$ENV{REMOTE_ADDR});
    #&chat::mail::login($OPT{name},$OPT{mail});
    &chat::frame;
} elsif ($OPT{mode} eq 'logoff') {
    $OPT{name} ||= $chat::member::noname;
    &chat::msglog::write($chat::member::master::name,(htescape $OPT{name}).$chat::member::master::goodbye,'','',$ENV{REMOTE_ADDR});
    &chat::goodbye(0);
  print STDOUT $FooterA0;
} elsif ($OPT{mode} eq 'loginform') {
  &chat::input::login;
  print STDOUT $FooterMin;
  print STDOUT q{<sw-ads-notes hidden></sw-ads-notes>};
} elsif ($OPT{mode} eq 'writeform') {
  $OPT{name} ||= $chat::member::noname;
  &chat::input::write;
  print STDOUT $FooterMin;
  print STDOUT q{<sw-ads-notes hidden></sw-ads-notes>};
} elsif ($OPT{mode} eq 'ranking') {
    &chat::myhead;
    &t3::printh(&chat::member::ranking::rank2html);
} elsif ($OPT{mode} eq 'usage') {
  &chat::myhead;
  $t3::foot = $SITE::about_html;
  &t3::printh(&chat::usage);
  print STDOUT $FooterA0;
} else {
  &chat::frame('login');
}
exit;

## -- Message Log
package chat::msglog;
use Chat::Util;

use Web::UserAgent::Functions qw(http_post); # <https://github.com/wakaba/perl-web-useragent-functions>
use AnyEvent;
use Encode;

sub irc ($) {
  my $prefix = $ENV{IRC_URL_PREFIX};
  my $channel = $ENV{IRC_CHANNEL};
  return unless defined $prefix and defined $channel;
  my $cv = AE::cv;
  http_post
      url => qq<$prefix/privmsg>,
      params => {
        channel => $channel,
        message => (decode 'euc-jp', $_[0]),
      },
      anyevent => 1,
      cb => sub {
        $cv->send;
      };
  $cv->recv;
} # irc

sub write {
  local($s{name},$s{msg},$s{dmy},$s{munou},$s{ip},$s{color},$s{msgcolor}) = @_;
  local(@new,@old,$i);  @new = ();  @old = ();
  open(LOG,$filename) || &SITE::error($filename.$chat::error::open);
    @old = <LOG>;
  close(LOG);
  
  ## Expire
  for ($i = 0; $i < $expire; $i++) {push (@new, $old[$i]);}
  
  my ($date,$name,$nline);
  $date = SITE::rfc822time(time, "+0900");
  if ($s{color}) {
    $s{color} = 'rgb('.int(rand(127)).','.int(rand(127)).','.int(rand(127)).')' if $s{color} eq 'random';
    $name = '<strong style="color:'.(htescape $s{color}).'">'.(htescape $s{name}).'</strong>';
  } else {
    $name = '<strong>'.(htescape $s{name}).'</strong>';
  }
  $s{msgcolor} = 'rgb('.int(rand(127)).','.int(rand(127)).','.int(rand(127)).')' if $s{msgcolor} eq 'random';
  $nline = $date.'<>'.$name.'<><>'.$s{msg}.'<>'.(htescape $s{msgcolor}).'<>'.(htescape $s{ip})."\n";
  unshift (@new, $nline);
  
  my $mline;
  if (($chat::munou::use == 1) && ($s{munou} ne 'on')) {
    #local($mline);
    $mline = &chat::munou::talk($s{name},$s{msg});
    if ($mline) {
      $date = SITE::rfc822time(time, "+0900");
      $mline = $date.'<>'.$chat::munou::showname.'<><>'.$mline."\n";
      unshift (@new, $mline);
    }
  }
  
  open(LOG,'> '.$chat::msglog::filename) || &SITE::error($filename.$chat::error::write);
    print LOG @new;
  close(LOG);
  
  ## Long log file.
  if ($chat::longlog::use == 1) {
    open(DATA,'>> '.$chat::longlog::filename) || &SITE::error($chat::longlog::filename.$chat::error::write);
      print DATA $nline;
      print DATA $mline if $mline;
    close(DATA);
    &chat::longlog::expire;
  }

  irc "$nline\n$mline";
}

sub show {
  ## Status.
  local($s,$email,$ename,$t);
  $ename = &chat::uriencode($main::OPT{name});
  $email = &chat::uriencode($main::OPT{mail});
  $t = "$chat::uri?reload=$main::OPT{reload}&amp;mode=$main::OPT{mode}&amp;munou=$main::OPT{munou}&amp;name=$ename&amp;mail=$email";
  $s = '<meta http-equiv="refresh" content="'.$reload.'; url='.$t.'">' if $main::OPT{reload} eq 'on';
  $s .= q{<script src="https://wiki.suikawiki.org/scripts/time" data-time-selector=time async></script>};
  $s .= '<body class="shows"><p class="ministatus"><a href="'.$t.'">'.$reloadt.'</a> '.$chat::member::title.' ';
  {
    my ($t,@u);
    ($t,@u) = &chat::member::list($main::ENV{REMOTE_ADDR},$main::OPT{name},$email);
    $s .= $t;
  }
  
  ## Messages
  {
    local(@lines);
    open(LOG, $filename) || &SITE::error($filename.$chat::error::open);
      @lines = <LOG>;
    close(LOG);
    $s .= &Msg2HTML(@lines);
  }
  $s .= $chat::html::chatlinks;
  
  &chat::myhead;
  $t3::foot = $SITE::about_html;
  &t3::printh($s);
}



## Msg -> HTML
## 
sub Msg2HTML {
  local(@DISPLOG) = @_;
  local(%L,$s,$t);
  
  foreach (@DISPLOG) {
    ($L{date},$L{name},$L{dmy},$L{msg},$L{msgcolor}) = split(/<>/, $_);
    if ($L{msg} =~ /(.+)\s*\|\|\s*(.+)\s*$/) {
      $L{msg} = $1;
      $t = $2;
      $L{name} =~ s/(<strong[^<>]*>)(.+?)(<\/strong>)/$1$t$3<!-- $2 -->/;
    }
    $L{msg} =~ s#\[\[([^\]]+)\]\]#<a href="http://suika.suikawiki.org/~wakaba/-temp/wiki/wiki?mycmd=read;mypage=@{[&chat::uri_encode ($1)]}" target=_top>$1</a>#g;
    $L{msg} = &chat::uri_link($L{msg});
    
    $s .= <<"_ONE_";
<p class="msg">
  <span class="name">$L{name}</span>&gt;
  <span class="body" style="color: $L{msgcolor}">$L{msg}</span>
  <span class="date">(@{[time2httime $L{date}]})</span>
</p>
_ONE_
    
  }
  $s;
}


package chat;

sub frame {
  local($mode) = @_;
  local(%uri);
  if ($mode eq 'login') {
    $uri{ue} = $uri.'?mode=loginform';
    $uri{shita} = $uri.'?mode=msg&amp;reload=on';
  } else {
    local(%e);
    $e{name} = &uriencode($main::OPT{name}); $e{mail} = &uriencode($main::OPT{mail});
    $e{color} = &uriencode($main::OPT{color});
    $uri{ue} = $uri."?mode=writeform&amp;reload=${main::OPT{reload}}&amp;name=${e{name}}&amp;mail=${e{mail}}&amp;munou=${main::OPT{munou}}&amp;color=${e{color}}";
    $uri{shita} = $uri."?mode=msg&amp;reload=${main::OPT{reload}}&amp;name=${e{name}}";
  }
  
  &myhead;
  local($s) = <<EOH;
<frameset rows="70,*">
<frame name="ue" src="${uri{ue}}&amp;$SITE::defuridata">
<frame name="shita" src="${uri{shita}}&amp;$SITE::defuridata">
</frameset>
$FooterMin
</html>
EOH
  &t3::printh($s);
}

sub uri_encode ($) {
  my $s = shift;
  $s =~ s/([^A-Za-z0-9_-])/sprintf '%%%02X', ord $1/ge;
  $s;
}

package chat::input;
use Chat::Util;

sub login {
  local($s);
  &SITE::GetCookie;
  $s = '<input type="checkbox" id="munou" name="munou" tabindex="4"><label for="munou">'.$munou.'</label> ' if $chat::munou::use;
  $s .= '<label for="color">'.$color.'</label>'.&colorlist('color',5,$main::OPT{color},$SITE::COOKIE{color});
  $s = <<EOH;
<div id="fst">
<div id="chat_input">
<form method="post" action="$chat::uri" accept-charset="ISO-2022-JP" target="_top">
<p class="fstline">
$SITE::defformdata
<input type="hidden" name="mode" value="login">
<label for="name">$name</label><input type="text" name="name" id="name" tabindex="1"
  value="@{[htescape $SITE::COOKIE{name}]}">
<label for="mail" lang="en">$mail</label><input type="text" name="mail" id="mail" value="@{[htescape $SITE::COOKIE{mail}]}" tabindex="2">
<input type="checkbox" id="reload" name="reload" tabindex="3" checked><label for="reload">$autoreload</label>
$s
<input type="submit" value="$login" id="in" tabindex="6">
</div>
</form>
</div>
EOH
  &chat::myhead;
  &t3::printh($s);
}

sub write {
  local($s,$ename);
  $ename = &chat::uriencode($main::OPT{name});
  local($s);
  $s = '<input type="checkbox" id="reload" name="reload" tabindex="2"';
  $s .= ' checked' if $main::OPT{reload} eq 'on';
  $s .= '><label for="reload">'.$autoreload.'</label> ';
  if ($chat::munou::use) {
    $s .= '<input type="checkbox" id="munou" name="munou" tabindex="3"';
    $s .= ' checked' if $main::OPT{munou} eq 'on';
    $s .= '><label for="munou">'.$munou.'</label> ' ;
  }
  $s .= '<label for="color">'.$color.'</label>'.&colorlist('color',4,$main::OPT{color});
  $s .= '<label for="msgcolor">'.$msgcolor.'</label>'.&colorlist('msgcolor',5);
  $s = <<EOH;
<body>
<div id="snd">
<div id="chat_input">
<form method="post" action="${chat::uri}" accept-charset="ISO-2022-JP" target="shita" name="send" id="send">
<p class="fstline">
<input type="hidden" name="mode" value="msg">
$SITE::defformdata
<input type="hidden" name="name" value="@{[htescape $main::OPT{name}]}">
<input type="hidden" name="mail" value="@{[htescape $main::OPT{mail}]}">
<label for="comment" id="myname">@{[htescape ${main::OPT{name}}]}&gt;</label>
<input type="text" name="comment" id="comment" tabindex="1">
$s
<input type="checkbox" id="autoclear" name="autoclear" checked tabindex="6"><label for="autoclear">${autoclear}</label>
<input type="submit" value="$say" id="say" tabindex="7">
<a href="$chat::uri?mode=msg&amp;name=${ename}&amp;comment=${chat::munou::eomikuji}&amp;$SITE::defuridata" tabindex="8" target="shita">$omikuji</a>
<a href="$chat::uri?mode=logoff&amp;name=$ename&amp;$SITE::defuridata" tabindex="9">$logoff</a>
</div>
</form>
</div>
<SCRIPT LANGUAGE="JavaScript" type="text/javascript" defer>
<!--
send = document.getElementById ("send");
function autoclear() {
  if (send.autoclear.checked) {
    if (send.comment) {
      send.comment.value = "";
    }
  }
}
function color_onchange() {
  myname.style.color = send.color.value;
}
function msgcolor_onchange() {
  send.comment.style.color = send.msgcolor.value;
}
function send_onsubmit() {
  setTimeout("autoclear()",10);
}

  send.onsubmit = send_onsubmit;
  send.color.onchange = color_onchange;
  send.msgcolor.onchange = msgcolor_onchange;
// -->
</SCRIPT>
EOH
  &chat::myhead;
  &t3::printh($s);
}


sub colorlist {
  local(%S,$t,$u,$j,$flag);
  ($S{name},$S{tabindex},$S{selitem}) = @_;
  
  for ($i = 0; $i < $#colors; $i = $i + 2) {
    $j = $i + 1;
    $u .= "<option value=\"$colors[$j]\" style=\"background-color: $colors[$j]\"";
    if ($S{selitem} eq $colors[$j] && !$flag) {
      $u .= " selected"; $flag = 1;
    }
    $u .= ">$colors[$i]\n";
  }
  $t = "<select name=\"$S{name}\" id=\"$S{name}\" tabindex=\"$S{tabindex}\">";
  $t .= '<option value=""';  $t .= ' selected' if !$flag;
  $t .= ">$defval\n$u</select>\n";
  $t;
}

=head1 AUTHOR

Wakaba <wakaba@suikawiki.org>.

=head1 LICENSE

Copyright 2000-2022 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
