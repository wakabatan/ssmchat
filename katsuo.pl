=head1 NAME

katsuo.pl - chat system via the HTTP

=cut

## -- Jinkou-munou part.

package chat::munou;

## $Ret = &chat::munou::GetMsg($Msg,$name,$words)
## 
## 	$Msg	Original message.  Munou reply to this msg.
## 	$name	Name of original messager.
## 	$words	Additional word list.  Comma separated.
## 	$Ret	Return.  Munou's message.
## 
sub GetMsg {
  local($inline,$name,$words) = @_;
  srand;

  ## -- Open brain file.
  &open_brain;
  sub open_brain {
    local($out);
    open(DATA,$brainfile) or &SITE::error("$brainfileを開けませんでした。");
      @LIST = <DATA>;
    close(DATA);
    
    foreach $line (@LIST) {
      chop($line);
      next if ($line =~ /^#/);                     # 注釈文
      if ($line =~ /^\@\{([^\}]*)\},(.*)/) {          # データ(@)読み取り、保存
        local($w) = $2;
        ($key = $1) =~ s/^(\d+)://;       #   ここでは:識別子(固定)は外す
        push(@KEY,$key);
        ($word{$key} = $w) && next;
      }
    }
    if ($#S >= 0) {$out = @S[int(rand(@S))];  return $out;}
    
    push(@KEY,"name");
    push(@KEY,"words");
    push(@KEY,"msg");
    
    $word{'name'} = $name;
    $word{'msg'} = $inline;
    $word{'words'} = $words;
    @KEY = sort {length($b)-length($a);} @KEY;
  }
  
  
  $inline = "{$inline}" if ($inline !~ /\{([^\}]*)\}/);
  
  if ($#FORMAT >= 0) {
    $_ = @FORMAT[int(rand(@FORMAT))];
  } else {
    $_ = &rndword_(&getkey($inline));
  }
  
  while (s/\{([^\}]*)\}/&rndword_($1)/eg) {;};
  $_;
}

## Get word at randam.  (Private)
## 
sub rndword_ {
  local($key) = @_;
  local(@WORD) = split(/,/,$word{$key});
  return if ($#WORD < 0);
  @WORD[int(rand(@WORD))];
}
sub getkey {
  local($req) = @_;
  foreach $one (@KEY) {return $one if ($req =~ /\Q$one\E/);}
  '';
}

## Add word to brain.
## 
## $Status = &chat::munou::addword($key,$word);
## 
## 	$key	Keyword.
## 	$word	Added word.
## 	$Status	Return.  Status message.
## 
sub addword {
  local($key,$word) = @_;
  return if $key eq '';
  return if $word eq '';
  
  open(DATA,$brainfile) or return $openerror;
    local(@LIST) = <DATA>;
  close(DATA);
  
  local($hit) = 0;
  open(DATA,'> '.$brainfile) or return $openerror;
    local($i);
    for ($i = 0;$i <= $#LIST;$i++) {
      chop($LIST[$i]);
      if ($LIST[$i] =~ /^\@\{\Q$key\E\},/) {
        $LIST[$i] = "$LIST[$i],$word";  $hit = 1;
      }
      print DATA $LIST[$i]."\n";
    }
    print DATA "\@\{$key\},$word\n" if !$hit;
  close(DATA);
  $added.$key.$kioku.$word;
}

## Delete word from brain.
## 
## $Status = &chat::munou::delword($key,$word);
## 
## 	$key	Keyword.
## 	$word	Deleted word.
## 	$Status	Return.  Status message.
## 
sub delword {
  local($key,$word) = @_;
  local($result) = $notfound.$key.$wasure.$word;
  
  open(DATA,$brainfile) or return $openerror;
    local(@LIST) = <DATA>;
  close(DATA);
  
  open(DATA,"> $brainfile") or return $openerror;
    local($i);
    for ($i = 0; $i <= $#LIST; $i++) {
      chop($LIST[$i]);
      if ($LIST[$i] =~ /^\@\{\Q$key\E\},/) {
        $LIST[$i] .= ",";
        if ($LIST[$i] =~ /,\Q$word\E,/) {
          $LIST[$i] =~ s/,\Q$word\E,/,/;
          $result = $deleted.$key.$wasure.$word;
        }
        $LIST[$i] =~ s/,$//;
      }
      if ($LIST[$i] =~ /^#/ || $LIST[$i] =~ /,/) {
        print DATA $LIST[$i]."\n";
      }
    }
  close(DATA);
  $result;
}

=head1 AUTHOR

Wakaba <w@suika.fam.cx>.

=head1 ACKNOWLEDGEMENT

This script is largely inspired by Comchat Web Chat System's
katsuo.pl.

=head1 COPYRIGHT

Copyright 2000-2010 Wakaba <w@suika.fam.cx>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
