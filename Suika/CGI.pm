
=pod

Suika::CGI
The Watermeron Project Web Server CGI Functions.

Copyright The Watermeron Project 2001.

=cut

package Suika::CGI;
our $VERSION = '1.1';
$var = '/home/wakaba/var/' unless $var;
$rooturi = 'http://'.$main::ENV{SERVER_NAME}.'/';
#require $var.'Suika-CGI.ph';
require Jcode;

&_getcookie;
&_getparams;

sub _getcookie {
  for (split /; ?/, $main::ENV{HTTP_COOKIE}) {
    my ($name, $value) = split /=/, $_, 2;
    $name =~ tr/ //d; $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack(C, hex($1))/eg;
    $value = Jcode->new ($value)
                  ->tr ("\xA3\xB0-\xA3\xB9\xA3\xC1-\xA3\xDA\xA3\xE1-\xA3\xFA\xA1\xF5\xA1\xA4\xA1\xA5\xA1\xA7\xA1\xA8\xA1\xA9\xA1\xAA\xA1\xAE\xA1\xB0\xA1\xB2\xA1\xBF\xA1\xC3\xA1\xCA\xA1\xCB\xA1\xCE\xA1\xCF\xA1\xD0\xA1\xD1\xA1\xDC\xA1\xF0\xA1\xF3\xA1\xF4\xA1\xF6\xA1\xF7\xA1\xE1\xA2\xAF\xA2\xB0\xA2\xB2\xA2\xB1\xA1\xE4\xA1\xE3\xA1\xC0\xA1\xA1" => q(0-9A-Za-z&,.:;?!`^_/|()[]{}+$%#*@='"~-><\ ))
                  ->euc;
    $cookie{$name} = $value;
  }
}

sub _getparams {
  my $argv;
  if ($main::ENV{REQUEST_METHOD} eq 'POST') {
    read(STDIN, $argv, $main::ENV{CONTENT_LENGTH});
  }
  $argv .= $main::ENV{QUERY_STRING};
  
  for (split /[&;]/, $argv) {
    my ($name, $value) = split /=/, $_, 2;
    $name =~ tr/ //d; $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack(C, hex($1))/eg;
    $value = Jcode->new ($value)
                  ->tr ("\xA3\xB0-\xA3\xB9\xA3\xC1-\xA3\xDA\xA3\xE1-\xA3\xFA\xA1\xF5\xA1\xA4\xA1\xA5\xA1\xA7\xA1\xA8\xA1\xA9\xA1\xAA\xA1\xAE\xA1\xB0\xA1\xB2\xA1\xBF\xA1\xC3\xA1\xCA\xA1\xCB\xA1\xCE\xA1\xCF\xA1\xD0\xA1\xD1\xA1\xDC\xA1\xF0\xA1\xF3\xA1\xF4\xA1\xF6\xA1\xF7\xA1\xE1\xA2\xAF\xA2\xB0\xA2\xB2\xA2\xB1\xA1\xE4\xA1\xE3\xA1\xC0\xA1\xA1" => q(0-9A-Za-z&,.:;?!`^_/|()[]{}+$%#*@='"~-><\ ))
                  ->euc;
    $param{$name} = $value;
  }
}

sub timetext {
  my $time = shift || time;
  my $mode = shift;
  my $timetext;
  $main::ENV{TZ} = 'JST-9';
  if ($mode eq 'simple') {
    my ($sec,$min,$hour,$day,$mon,$year,undef) = localtime($time);
    $year+= 1900; $mon++;
    $timetext = $year.'/'.$mon.'/'.$day.' '.$hour.':'.$min.':'.$sec;
  } else {
    $timetext = localtime($time);
  }
  $timetext;
}

sub setcookie {
  my ($opt, %param, $c) = @_;
  for (keys %param) {
    $c .= $_.'='.Suika::CGI::Encode::uri($param{$_}).'; ' if $param{$_};
  }
  print STDOUT 'Set-Cookie: '.$c." expires=Sat, 01 Feb 2030 00:00:00 GMT\n" if $c && !$Suika::CGI::_noheader;
}

package Suika::CGI::Counter;

sub get {
  my ($id) = shift || 'default';  $id =~ tr/\x0d\x0a\x1f//d;
  my ($ret);
  my $cfile = shift || $file;
  open COUNT, $cfile or Suika::CGI::Error::die('open', file => $cfile);
    while (<COUNT>) {
      if (/^${id}\x1f(\d+)/) {
        $ret = $1;
        last;
      }
    }
  close COUNT;
  $ret;
}

sub up {
  my ($id) = shift || 'default';  $id =~ tr/\x0d\x0a\x1f//d;
  my $f = 0;
  my $cfile = shift || $file;
  open COUNT, $cfile; # or Suika::CGI::Error::die('open', file => $cfile);
    my @COUNT = <COUNT>;
  close COUNT;
  
  my $ret;
  open COUNT, '> '.$cfile or Suika::CGI::Error::die('write', file => $cfile);
    for (@COUNT) {
      if (/^\Q${id}\E\x1f(\d+)/) {
        $_ = $id."\x1f".($1+1)."\n";
        $ret = $1+1; $f = 1;
      }
      print COUNT $_;
    }
    unless ($f) {
      print COUNT $id."\x1f1\n";
      $ret = 1;
    }
  close COUNT;
  $ret;
}

package Suika::CGI::Log;


sub write {
  my %log;
  $log{text} = shift;
  return unless $log{text};
  $log{time} = time;
  
  if ($Suika::CGI::User::ID) {
    $log{name} = $Suika::CGI::User::ID;
    my $name = &Suika::CGI::User::ID2Name($log{name});
    $log{name} .= ' ('.$name.')' if $name;
  } else {
    for (sort (keys %main::ENV)) {
     $log{env} .= ' '.$_."\x1f".$main::ENV{$_}."\x1f"
        if $_ =~ /^(?:HTTP|REMOTE)/;
     }
  }
  
  open LOG, '>> '.$file or die("Can't write log. ".$!.$file);
    print LOG $log{time}."\x1f ";
    print LOG ($main::ENV{REMOTE_ADDR} || $main::ENV{REMOTE_HOST})."\x1f ";
    print LOG $log{name}."\x1f ";
    print LOG $log{text}."\x1f ";
    print LOG ($log{env} || 'HTTP_REFERER'."\x1f".$main::ENV{HTTP_REFERER}."\x1f".'HTTP_USER_AGENT'."\x1f".$main::ENV{HTTP_USER_AGENT}."\x1f".'HTTP_COOKIE'."\x1f".$main::ENV{HTTP_COOKIE})."\n";
  close LOG;
}

sub updated {
  my ($now, %d, @LIRS, $f) = time;
  ($d{uri}, $d{title}, $d{author}) = @_;
  open (LIRS, $lirs); #or Suika::CGI::Error::die('open', file => $lirs);
    @LIRS = <LIRS>;
  close LIRS;
  my $now2 = $now-10;
  for (@LIRS) {
    if (/^LIRS,\d+,\d+,\d+,\d+,$d{uri},/) {
      $_ = "LIRS,${now},${now2},32400,0,$d{uri},$d{title},$d{author},${SITE::mainuri},\n";
      $f = 1;
    }
  }
  if (!$f) {
    push @LIRS, "LIRS,${now},${now2},32400,0,$d{uri},$d{title},$d{author},${Suika::CGI::rooturi},\n";
  }
  open (LIRS, '> '.$lirs) or Suika::CGI::Error::die('write', file => $lirs);
    print LIRS @LIRS;
  close LIRS;
}

sub visit {
  package Suika::CGI::Counter;
  my $id = shift || '_default';
  my $file = $Suika::CGI::Log::visitdir.$id;
  my $ret = Suika::CGI::Counter::up('_visit', $file);
  Suika::CGI::Counter::up('Referer: '.$main::ENV{HTTP_REFERER}, $file)
    if $main::ENV{HTTP_REFERER};
  Suika::CGI::Counter::up('User-Agent: '.$main::ENV{HTTP_USER_AGENT}, $file)
    if $main::ENV{HTTP_USER_AGENT};
  Suika::CGI::Counter::up('User: '.$Suika::CGI::User::ID, $file);
  $ret;
}

package Suika::CGI::User;
&_getuserid();

sub _getuserid {
  $ID = $Suika::CGI::cookie{HnsClientID};
  unless ($ID) {
    $ID = &_createruri();
    print STDOUT "Set-Cookie: HnsClientID=$ID; expires=Sat, 01 Feb 2030 00:00:00 GMT; path=/\n" if !$Suika::CGI::_noheader;
  }
}

sub _createruri {
  my ($prefix, $sec, $pid, $rand) = ('RURI', time, $$ % 0x10000, rand(16));
  return sprintf('%s%08x%04x%01x', $prefix, $sec, $pid ,$rand)
}


sub ID2Name {
  my $name = shift;
  if (-f $file) {
    open MAP, $file or Suika::CGI::Error::die('open', file => $file);
    while (<MAP>) {
      if (/^(RURI\w+)\s+(.+)\s*/) {
        my $v = $2;
        if ($name eq $1) {close MAP;  return $v}
      }
    }
    close MAP;
  } else {
    Suika::CGI::Error::die('open', file => $file);
  }
  undef;
}

sub registname {
  my ($ruri, $name) = @_;
  return 0 if &ID2Name($ruri);
  open (RMAP, '>> '.$file) or return 0;
    print RMAP $ruri.' '.$name."\n";
  close RMAP;
  1;
}

package Suika::CGI::Encode;

sub uri {
  my $s = shift;
  my $r = shift || qr#[^A-Za-z0-9/:\@\$,_.!~*()-]#;
    $r = Jcode->new ($r, 'euc')
                  ->tr ("\xA3\xB0-\xA3\xB9\xA3\xC1-\xA3\xDA\xA3\xE1-\xA3\xFA\xA1\xF5\xA1\xA4\xA1\xA5\xA1\xA7\xA1\xA8\xA1\xA9\xA1\xAA\xA1\xAE\xA1\xB0\xA1\xB2\xA1\xBF\xA1\xC3\xA1\xCA\xA1\xCB\xA1\xCE\xA1\xCF\xA1\xD0\xA1\xD1\xA1\xDC\xA1\xF0\xA1\xF3\xA1\xF4\xA1\xF6\xA1\xF7\xA1\xE1\xA2\xAF\xA2\xB0\xA2\xB2\xA2\xB1\xA1\xE4\xA1\xE3\xA1\xC0\xA1\xA1" => q(0-9A-Za-z&,.:;?!`^_/|()[]{}+$%#*@='"~-><\ ))
                  ->jis;
  $s =~ s/($r)/sprintf('%%%02X', ord($1))/eg;
$s;
}

1;
