=head1 NAME

chat system via the HTTP -- common module

=cut

our $version = q[ssmchat];

=head1 CAUTION!

This script may be going to a free software.  But currently
is including non-free code.  `LICENSE' section is not cover
that code.

=cut


## -- Jinkou Munou
package chat::munou;
use Chat::Util;

## Get munou's return.
## 
sub talk {
  local($fname,$fcomment) = @_;
  $fname = htescape $fname;
  $fcoment = htescape $fcomment;
  
  if ($study) {
    if ($fcomment =~ /(.+)$kioku(.+)/) {
      return &addword($1,$2);
    } elsif ($fcomment =~ /(.+)$wasure(.+)/) {
      return &delword($1,$2);
    }
  }
  
  if ($#sleep >= 0) {
    local($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst);
    ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    foreach $no_hour (@sleep) {
      if ($no_hour == $hour) {
        return $sleeping if $fcomment =~ /$name/;
      }
    }
  }
  
  local(@sn,$sns,@member);
  open(LOG,$chat::member::filename) || &SITE::error($chat::member::filename.$chat::error::open);
    seek(LOG,0,0); local(@member) = <LOG>;
  close(LOG);
  for ($i = 0; $i <= $#member; $i++) {
    @sn = split(/<>/, $member[$i]);
    $sns = $sns.$sn[1].',' if ($sn[1]);
  }
  $sns =~ s/,$//;
  if ($fcomment !~ /$name/ && int(rand(100)) > $katsuo_p) {
    return if ($sns =~ /,/);
  }
  
  if ($sns =~ /,/) {$sns = "$sns,$name";}
  else             {$sns = $name;}
  $fname =~ s/.*\s(\S+)/$1/;
  $ret = &GetMsg($fcomment,$fname,$sns);
  if (length($ret) < 2) {
    if ($fcomment !~ /$name/ && int(rand(100)) > $katsuo_h) {
      return if ($sns =~ /,/);
    }
    $ret = &GetMsg('{\xCA\xD6\xBB\xF6\xBB\xEC}',$fname,@SN);
  } else {
    $ret = $ret.'-&gt; '.$fname if $ret !~ /\|\|/;
  }
  $ret;
}


## $Ret = &chat::munou::GetMsg($Msg,$name,$words)
## 
## 	$Msg	Original message.  Munou reply to this msg.
## 	$name	Name of original messager.
## 	$words	Additional word list.  Comma separated.
## 	$Ret	Return.  Munou's message.
## 
sub GetMsg {
  local($inline,$name,$words) = @_;
  srand;

  ## -- Open brain file.
  &open_brain;
  sub open_brain {
    local($out);
    open(DATA,$brainfile) or &SITE::error($brainfile.$chat::error::open);
      @LIST = <DATA>;
    close(DATA);
    
    foreach $line (@LIST) {
      chop($line);
      next if ($line =~ /^#/);
      if ($line =~ /^\@\{([^\}]*)\},(.*)/) {
        local($w) = $2;
        ($key = $1) =~ s/^(\d+)://;
        push(@KEY,$key);
        ($word{$key} = $w) && next;
      }
    }
    if ($#S >= 0) {$out = @S[int(rand(@S))];  return $out;}
    
    push(@KEY,"name");
    push(@KEY,"words");
    push(@KEY,"msg");
    
    $word{'name'} = $name;
    $word{'msg'} = $inline;
    $word{'words'} = $words;
    @KEY = sort {length($b)-length($a);} @KEY;
  }
  
  
  $inline = "{$inline}" if ($inline !~ /\{([^\}]*)\}/);
  

  if ($#FORMAT >= 0) {
    $_ = @FORMAT[int(rand(@FORMAT))];
  } else {
    $_ = &rndword_(&getkey($inline));
  }
  
  while (s/\{([^\}]*)\}/&rndword_($1)/eg) {;};
  $_;
}

## Get word at randam.  (Private)
## 
sub rndword_ {
  local($key) = @_;
  local(@WORD) = split(/,/,$word{$key});
  return if ($#WORD < 0);
  @WORD[int(rand(@WORD))];
}
sub getkey {
  local($req) = @_;
  foreach $one (@KEY) {return $one if ($req =~ /\Q$one\E/);}
  '';
}

## Add word to brain.
## 
## $Status = &chat::munou::addword($key,$word);
## 
## 	$key	Keyword.
## 	$word	Added word.
## 	$Status	Return.  Status message.
## 
sub addword {
  local($key,$word) = @_;
  return if $key eq '';
  return if $word eq '';
  
  open(DATA,$brainfile) or return $openerror;
    local(@LIST) = <DATA>;
  close(DATA);
  
  local($hit) = 0;
  open(DATA,'> '.$brainfile) or return $openerror;
    local($i);
    for ($i = 0;$i <= $#LIST;$i++) {
      chop($LIST[$i]);
      if ($LIST[$i] =~ /^\@\{\Q$key\E\},/) {
        $LIST[$i] = "$LIST[$i],$word";  $hit = 1;
      }
      print DATA $LIST[$i]."\n";
    }
    print DATA "\@\{$key\},$word\n" if !$hit;
  close(DATA);
  $added.$key.$kioku.$word;
}

## Delete word from brain.
## 
## $Status = &chat::munou::delword($key,$word);
## 
## 	$key	Keyword.
## 	$word	Deleted word.
## 	$Status	Return.  Status message.
## 
sub delword {
  local($key,$word) = @_;
  local($result) = $notfound.$key.$wasure.$word;
  
  open(DATA,$brainfile) or return $openerror;
    local(@LIST) = <DATA>;
  close(DATA);
  
  open(DATA,"> $brainfile") or return $openerror;
    local($i);
    for ($i = 0; $i <= $#LIST; $i++) {
      chop($LIST[$i]);
      if ($LIST[$i] =~ /^\@\{\Q$key\E\},/) {
        $LIST[$i] .= ",";
        if ($LIST[$i] =~ /,\Q$word\E,/) {
          $LIST[$i] =~ s/,\Q$word\E,/,/;
          $result = $deleted.$key.$wasure.$word;
        }
        $LIST[$i] =~ s/,$//;
      }
      if ($LIST[$i] =~ /^#/ || $LIST[$i] =~ /,/) {
        print DATA $LIST[$i]."\n";
      }
    }
  close(DATA);
  $result;
}

## -- Chat members
package chat::member;
use Chat::Util;

## ($members(HTML),@members) = &chat::member::list($myIP, [$myname]);
## Get member list.
## 
sub list {
   my ($myip,$myname,$myemail) = @_;
   my (@members, @smem,$flag,%L);
   open LOG, $filename || &SITE::error($filename. $chat::error::open);
     @members = <LOG>;
   close LOG;
   
   unless ($myname) {
     &SITE::GetCookie ();  $myname = $SITE::COOKIE{name};
   }
   $myname = htescape $myname;
   $myemail = htescape $myemail;
   
   my ($times) = time ();  $flag = 1;
   foreach my $line (@members) {
     ($L{time},$L{name},$L{ip},$L{email}) = split(/<>/, $line);
     if ($times-120 > $L{time}) {$line = ''; next;}
     if (($L{name} eq $myname) && ($L{ip} eq $myip) && $flag) {
       $line = "$times<>$myname<>$myip<>$myemail<>\n";
       $flag = 0;
     }
     if ($L{email}) {
       push(@smem, '<a href="mailto:'.$L{email}.'">'.$L{name}.'</a><span class="desc">('.$L{ip}.')</span>'.$spliter);
     } else {
       push(@smem, $L{name}.'<span class="desc">('.$L{ip}.')</span>'.$spliter);
     }
   }
   
   ## New member.
   if ($flag) {
     push(@members, "$times<>$myname<>$myip<>$myemail<>\n");
     push(@smem, $myname.'<span class="desc">('.$myip.')</span>'.$spliter);
   }
   
   open(LOG,'> '.$filename) || &SITE::error($filename.$chat::error::write);
     print LOG @members;
   close(LOG);
   
   ## Munou
   if ($chat::munou::use) {
     push(@members, "<>$chat::munou::name<><>d\n");
     push(@smem, $chat::munou::name.$chat::munou::info);
   }
   ("@smem",@members);
}

## -- Chat ranking.
package chat::member::ranking;

sub add {
  local(@rank,$rank,$rflag,%R,@rankN);
  local($nowdate) = time;
  open(IN, $filename) || &SITE::error($filename.$chat::error::open);
    @rank = <IN>;
  close(IN);
  
  $rflag = 0;
  foreach $rank (@rank) {
    ($R{name}, $R{count}, $R{date}) = split(/<>/, $rank);
    next if ($nowdate - $R{date} > $limits);
    if ($R{name} eq $_[0]) {
      $rflag = 1; $R{count}++;
      $rank = "$R{name}<>$R{count}<>$nowdate<>\n";
    }
    push(@rankN, $rank);
  }
  unshift(@rankN, "$_[0]<>1<>$nowdate<>\n") if ($rflag == 0);
  
  open(OUT, '> '.$filename) || &SITE::error($filename.$chat::error::write);
    print OUT @rankN;
  close(OUT);
}

## Other modules.
package chat;

sub goodbye ($) {
  my ($full) = shift;
  &myhead;
  my ($s) = '<body class="goodbye">';
  $s .= '<h1>'.$title.'</h1>' if $full;
  $s .= $goodbye;
  $t3::foot = $SITE::about_html if $full;
  &t3::printh($s);
}

=head1 chat::uri_link($s)

URI -> HTML anchor converter.

Note: This code was written by Oozaki-san, the author of
Perl memo <http://www.din.or.jp/~ohzaki/perl.htm>.  Feel
free to cite or to copy, but don't remove this source URI.

FIXME: SGML's & entity support.  This script doesn't work
correct in current implemention.

=cut

sub uri_link ($) {
  my $str = shift;
  my $tag_regex_ = q{[^"'>]*(?:"[^"]*"[^"'>]*|'[^']*'[^"'>]*)*}; #'}}}}
  my $comment_reg = '<!(?:--[^-]*(?:(?!--)-[^-]*)*--(?:(?!--)[^>])*)*(?:>|(?!\n)$|--.*$)';
  my $tag_regex = qq{$comment_reg|<$tag_regex_>};
  my $http_URL_regex =
q{\b(?:https?|shttp)://(?:(?:[a-zA-Z0-9](?:[-a-zA-Z0-9]*[a-zA-Z0-9])} .
q{?\.)*[a-zA-Z](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?\.?|[0-9]+\.[0-9]+\.[0-9} .
q{]+\.[0-9]+)(?::[0-9]*)?(?:/(?:[-_.!~*'()a-zA-Z0-9:@=+$,]|&amp;|%[0-9A-F} .
q{a-f][0-9A-Fa-f])*(?:;(?:[-_.!~*'()a-zA-Z0-9:@=+$,]|&amp;|%[0-9A-Fa-f][0} .
q{-9A-Fa-f])*)*(?:/(?:[-_.!~*'()a-zA-Z0-9:@=+$,]|&amp;|%[0-9A-Fa-f][0-9A-} .
q{Fa-f])*(?:;(?:[-_.!~*'()a-zA-Z0-9:@=+$,]|&amp;|%[0-9A-Fa-f][0-9A-Fa-f])} .
q{*)*)*(?:\?(?:[-_.!~*'()a-zA-Z0-9;/?:@=+$,]|&amp;|%[0-9A-Fa-f][0-9A-Fa-f} .
q{])*)?)?(?:#(?:[-_.!~*'()a-zA-Z0-9;/?:@=+$,]|&amp;|%[0-9A-Fa-f][0-9A-Fa-} .
q{f])*)?(?![-_.!~*'()a-zA-Z0-9;/?:@=+$,#]|&amp;)}; #'}}
  my $ftp_URL_regex =
q{\bftp://(?:(?:[-a-zA-Z0-9_.!*'();=~]|&amp;|%[0-9A-Fa-f][0-9A-Fa-f])*(?:} .
q{:(?:[-a-zA-Z0-9_.!*'();=~]|&amp;|%[0-9A-Fa-f][0-9A-Fa-f])*)?@)?(?:(?:[a} .
q{-zA-Z0-9](?:(?:[a-zA-Z0-9]|-)*[a-zA-Z0-9])?\.)*[a-zA-Z](?:(?:[a-zA} .
q{-Z0-9]|-)*[a-zA-Z0-9])?|[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)(?::[0-9]*)} .
q{?(?:/(?:[-a-zA-Z0-9_.!*'():@=~]|&amp;|%[0-9A-Fa-f][0-9A-Fa-f])*(?:/(?:[} .
q{-a-zA-Z0-9_.!*'():@=~]|&amp;|%[0-9A-Fa-f][0-9A-Fa-f])*)?(?:;type=[AIDai} .
q{d])?)?(?![-a-zA-Z0-9_.!*'():@=~/]|&amp;)}; #'}
  my $mail_regex =
q{(?:[^(\040)<>@,;:".\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\} .
q{\[\]\000-\037\x80-\xff])|"[^\\\x80-\xff\n\015"]*(?:\\[^\x80-\xff][} .
q{^\\\x80-\xff\n\015"]*)*")(?:\.(?:[^(\040)<>@,;:".\\\[\]\000-\037\x} .
q{80-\xff]+(?![^(\040)<>@,;:".\\\[\]\000-\037\x80-\xff])|"[^\\\x80-\\} .
q{xff\n\015"]*(?:\\[^\x80-\xff][^\\\x80-\xff\n\015"]*)*"))*@(?:[^(\0} .
q{40)<>@,;:".\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\[\]\000} .
q{-\037\x80-\xff])|\[(?:[^\\\x80-\xff\n\015\[\]]|\\[^\x80-\xff])*\])} .
q{(?:\.(?:[^(\040)<>@,;:".\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;} .
q{:".\\\[\]\000-\037\x80-\xff])|\[(?:[^\\\x80-\xff\n\015\[\]]|\\[^\x} .
q{80-\xff])*\]))*};
  my $text_regex = q{[^<]*};
  
  my $result = '';  my $skip = 0;
while ($str =~ /($text_regex)($tag_regex)?/gso) {
  last if $1 eq '' and $2 eq '';
  my $text_tmp = $1;
  my $tag_tmp = $2;
  if ($skip) {
    $result .= $text_tmp . $tag_tmp;
    $skip = 0 if $tag_tmp =~ /^<\/[aA](?![0-9A-Za-z])/;
  } else {
    $text_tmp =~ s/($http_URL_regex)/<A HREF="$1">$1<\/A>/go;
    $text_tmp =~ s/((?:mailto\:)?)($mail_regex)/<A HREF="mailto:$2">$1$2<\/A>/go;
    $text_tmp =~ s/($ftp_URL_regex)/<A HREF="$1">$1<\/A>/go;
    $result .= $text_tmp . $tag_tmp;
    $skip = 1 if $tag_tmp =~ /^<[aA](?![0-9A-Za-z])/;
    if ($tag_tmp =~ /^<(XMP|PLAINTEXT|SCRIPT)(?![0-9A-Za-z])/i) {
      $str =~ /(.*?(?:<\/$1(?![0-9A-Za-z])$tag_regex_>|$))/gsi;
      $result .= $1;
    }
  }
}
   $result;
}

=head1 AUTHOR

Wakaba <w@suika.fam.cx>.

=head1 COPYRIGHT

Copyright 2000-2010 Wakaba <w@suika.fam.cx>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
