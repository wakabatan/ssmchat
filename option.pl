=head1 NAME

chat system via the HTTP
* option.pl -- chat room defination options

=cut

package chat;
  
  $title = 'む*す茶';	## chat title ([^<>]+)
  $id = 'mususu';	## chat id ([A-Za-z0-9_]+)
  
  $chat::file::chat2 = './chat2.cgi';	## chat2.cgi
  
  $main::chat_title = $title;
  $main::chatid = $id;
  $main::script = './comchatq.cgi';
  
  
## -- Users
package chat::member;
  
  $filename = './member.dat';
  $spliter = ', ';
  $title = '参加者';
  $pointer = '*';
  $noname = "名無しさん";
  
  $main::memfile = $filename;
  $main::rom_disp = 3;
  $main::rommark = "($main::ENV{'REMOTE_ADDR'})";
  
  ## -- Master (System message)
  package chat::member::master;
    $name = '天の声';
    $color = 'gray';
    $pointer = ' ';
    
    $welcome = 'さん、こんにちは。';
    $goodbye = 'さん、さようなら。';
  
  ## -- Ranking
  package chat::member::ranking;
    $use = 0;	## 1[use] or 0[not use]
    if ($use) {
      $filename = './rank.dat';
      $limit = 60;	## (days) Keeping.
        $limits = $limit * (24 * 60 * 60);
      $name = '暇人ランキング';
      $reportto = 'wakaba@suikawiki.org';
    }
    ## obs.
    $chat::rank::use = $use;
    if ($use) {
      $chat::rank::logfile = $filename;
      $chat::rank::limit = $limit;
      $chat::rank::name = $name;
      $chat::rank::reportto = $reportto;
    }
  
## -- Logging
  
  ## -- Late log
  package chat::msglog;
    $filename = './comchat.log';
    $reload = 30;	## sec.
    $expire = 39;	## lines.
    $reloadt = '最新';
    
    $main::logfile = $filename;
    $htmlbasename = './chatlog.html';	## chatlog.html.*
    $chat::file::chathtml = $htmlbasename;
  
  ## -- Long log
  package chat::longlog;
    $use = 1;	## 1(use) or 0(not use)
    if ($use) {
      $filename = './comchat_.log';
      $expire = 600;	## lines.
      
      $main::backlog = $use;
      $main::backlogfile = $filename;
      $main::w_log_clear_lines = $expire;
    }

## -- Jinkou munou
package chat::munou;
  
  $use = 1;	## 0(use) or 1(non-use).
  if ($use) {
    $filename = './katsuo.dat';
    $brainfile = $filename;
    
    $name = 'む*す';
    $showname = '<strong style="color: rgb('.int(rand(127)).','.int(rand(127)).','.int(rand(127)).')">'.$name.'</strong>';
    $pointer = '<a href="mailto:wakaba@suikawiki.org" style="color: #FF0080">+</a>';
    $info = '<a href="http://suika.suikawiki.org/~wakaba/-temp/wiki/wiki?%A4%E0%2A%A4%B9" class="desc" title="人工無能" target=_top>(255.255.255.2)</a>.';
    
       $katsuo_p = 90;            # 応答する確率(%)
       $katsuo_h = 10;            # 応答がない場合に適当な返事をする確率(%)
       #@sleep = (21,23);    # 動作させない時間帯(常時動作の場合comment化)
       $sleeping = $name.'は鼾をかいて眠っています。';
       
       $study = 1;         # 教育機能の使用(1=使用)
       $kioku = '===';	## 教育に使用するkey(○○key△△と使用する。)
       $wasure = '!==';
       
       $openerror = '脳みそを開けませんでした。';
       $added = '少し賢くなりました。こんなことも分かります→ ';
       $deleted = '最近物忘れが激しくてね。なんだっけ? これ→ ';
       $notfound = 'てか、元から知りません→ ';
       
     $ename = &chat::uriencode($name);
     $eomikuji = '%1B%24B%24%2A%24_%24%2F%248%1B%28B -> '.$ename;
     	## URI encoded 'omikuji' command.
       
       $omijuji = 1;	## 御神籤button(1=使用)
  }

## -- Admin
package chat::admin;
  $name = 'わかば';	## Administrator

## -- Error Message
package chat::error;
  $open = 'を開けませんでした。';
  $write = 'に書き込めません。';
  $sendmail = '管理用メイルの送信が行えません。';

## -- HTML fragments
package chat::html;
  
  $chatlinks = <<EOH;
<p>
[<a href="${chat::uri}?mode=usage">使い方</a>]
<!--[<a href="${chat::uri}?mode=ranking">発言数</a>]
[<a href="${chat::uri}">管理</a>]-->
</p>
<address>
[<a href="http://suika.suikawiki.org/" title="このサーバーの首頁" target=_top>/</a>
<a href="http://suika.suikawiki.org/map" title="このサーバーの案内" target=_top>地図</a>
<a href="http://suika.suikawiki.org/search/" title="このサーバーの検索" target=_top>検索</a>]
</address>
EOH

## -- Form Message
package chat::input;
  $name = '名前';
  $mail = 'メールアドレス';
  $autoreload = '自動更新';
  $login = '入室';
  $say = '発言';
  $munou = $chat::munou::name.'を無視';
  $logoff = '退室';
  $omikuji = 'おみくじ';
  $autoclear = '自動消去';
  
  ## -- Color
    $color = '名前色';
    $msgcolor = '発言色';
    $defval = '(既定値)';
    @colors = (
	'黒','#000000', '赤','#FF0000', '橙','#FF8000', '黄','yellow', '緑','#00FF00', '青','#0000FF',  '藍','#00008B','紫','#8A2BE2',
	'超薄水','#F0F8FF', 'azure','#F0FFFF', '薄水','#7FFFDF', '水','#00FFFF', 'シアン','#00FFFF', darkturquoise,'#00CED1', '空','#00BFFF', dodgerblue,'#1E90FF',
	'蛍光青','#6495ED', '濃青','#483D8B',
	'青緑','#5F9EA0', darkseagreen,'#8FBC8F', '薄緑','#008B8B', '濃緑','#006400', darkslategray,'#2F4F4F',
	darkolivegreen,'#556B2F', 'ベイジュ','#F5F5DC', '黄緑','#7FFF00', '蛍光黄緑','#00FF80',
	'超薄肌','#FAEBD7', '絹','#FFF8DC', blanchedalmond,'#FFEBCD', '薄肌','#FFE4C4', '木','#DEB887', '茶色','#A52A2A',
	darkkhaki,'#BDB76B', '黄土','#B8860B', 
	darkorange,'#FF8C00'  ,
	'チョコレイト','#D2691E', '赤橙','#FF7550',
	'紅','#DC143C', '暗赤','#8B0000',
	deeppink,'#FF1493', 'ピンク','#FF80FF', '桃','#FFC0CB', '濃鮭','#E9967A',
	'赤紫','#8B008B', '青紫','#A040FF', '薄紫', '#9932CC', darkviolet,'#9400D3',
	
	'灰','#A9A9A9', dimgray,'#696969', '白', '#FFFFFF', 
	
	'firebrick', '#B22222', 'floralwhite', '#FFFAF0', 'forestgreen', '#228B22', 'fuchsia', '#FF00FF', 'gainsboro', '#DCDCDC', 'ghostwhite', '#F8F8FF', '金', '#FFD700', 'goldenrod', '#DAA520', '灰', '#808080', '緑', '#008000', 'greenyellow', '#ADFF2F', 'honeydew', '#F0FFF0', 'hotpink', '#FF69B4', 'indianred', '#CD5C5C ', 'indigo', '#4B0082', 'ivory', '#FFFFF0', 'khaki', '#F0E68C', 'lavender', '#E6E6FA', 'lavenderblush', '#FFF0F5', 'lawngreen', '#7CFC00', 'lemonchiffon', '#FFFACD', 'lightblue', '#ADD8E6', 'lightcoral', '#F08080', 'lightcyan', '#E0FFFF', 'lightgoldenrodyellow', '#FAFAD2', 'lightgreen', '#90EE90', 'lightgrey', '#D3D3D3', 'lightpink', '#FFB6C1', 'lightsalmon', '#FFA07A', 'lightseagreen', '#20B2AA', 'lightskyblue', '#87CEFA', 'lightslategray', '#778899', 'lightsteelblue', '#B0C4DE', 'lightyellow', '#FFFFE0', 'lime', '#00FF00', 'limegreen', '#32CD32', 'linen', '#FAF0E6', 'マゼンタ', '#FF00FF', 'maroon', '#800000', 'mediumaquamarine', '#66CDAA', 'mediumblue', '#0000CD', 'mediumorchid', '#BA55D3', 'mediumpurple', '#9370DB', 'mediumseagreen', '#3CB371', 'mediumslateblue', '#7B68EE', 'mediumspringgreen', '#00FA9A', 'mediumturquoise', '#48D1CC', 'mediumvioletred', '#C71585', 'midnightblue', '#191970', 'mintcream', '#F5FFFA', 'mistyrose', '#FFE4E1', 'moccasin', '#FFE4B5', 'navajowhite', '#FFDEAD   ', 'navy', '#000080', 'oldlace', '#FDF5E6', 'オリーブ', '#808000', 'olivedrab', '#6B8E23', '橙', '#FFA500', 'orangered', '#FF4500', 'orchid', '#DA70D6', 'palegoldenrod', '#EEE8AA', 'palegreen', '#98FB98', 'paleturquoise', '#AFEEEE', 'palevioletred', '#DB7093', 'papayawhip', '#FFEFD5', 'peachpuff', '#FFDAB9', 'peru', '#CD853F', '桃', '#FFC0CB', 'plum', '#DDA0DD', 'powderblue', '#B0E0E6  ', 'purple', '#800080', '赤', '#FF0000', 'rosybrown', '#BC8F8F', 'royalblue', '#4169E1', 'saddlebrown', '#8B4513', 'salmon', '#FA8072', 'sandybrown', '#F4A460', 'seagreen', '#2E8B57', 'seashell', '#FFF5EE', 'sienna', '#A0522D', '銀', '#C0C0C0', '空', '#87CEEB', 'slateblue', '#6A5ACD', 'slategray', '#708090', '雪', '#FFFAFA', 'springgreen', '#00FF7F', 'steelblue', '#4682B4', 'tan', '#D2B48C', 'teal', '#008080', 'thistle', '#D8BFD8', 'トマト', '#FF6347', 'turquoise', '#40E0D0', '菫', '#EE82EE', 'wheat', '#F5DEB3', '?', '#F5F5F5', '金', '#FFFF00', '黄緑', '#9ACD32', 
	
	'無作為','random'
    );
    
    *chat::html::colors = \@colors;
    *chat::html::defval = \$defval;


package main;

$chat::goodbye = <<"EOH";
   <p class="chat_bye">御利用有難う御座居ました。</p>
   <ul class="chat_linklist">
   <li><a href="http://suika.suikawiki.org/~wakaba/" target="_top">若葉的世界</a></li>
   <li><a href="http://members.tripod.co.jp/reiweb/" 
       target="_top">Yuukiのパソコン日記</a></li>
   <li><a href="http://www.micnet.ne.jp/takamatu/" 
       target="_top">りんご博物館</a></li>
   </ul>

[<a href="http://suika.suikawiki.org/" title="このサーバーの首頁" target=_top>/</a>
<a href="http://suika.suikawiki.org/map" title="このサーバーの案内" target=_top>地図</a>
<a href="http://suika.suikawiki.org/search/" title="このサーバーの検索" target=_top>検索</a>]

EOH
*w_taishitsu_msg = \$chat::goodbye;


sub myheads {
## HTML header
$t3::hhead = <<"EOH";
<!DOCTYPE html PUBLIC "-//somebody//DTD-ha-nai FUSHIGI Markup">
<html lang="ja">
<head>
<link rel="copyright" href="https://suika.suikawiki.org/c/gpl">
<style type="text/css">
\@import '${SITE::default_css}';
</style>
<title>${chat_title}</title>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-24580106-1', 'auto');
  ga('send', 'pageview');
</script>
EOH
}
*chat::myhead = \&myheads;

sub chat::usage () {
  my ($s) = <<"EOM";
<body class="usage">
<h2>利用上の御注意</h2>

<ul>
  <li>常識から外れた言動は自粛して下さい。</li>
  <li>各自の発言の責任は発言者に属するものとします。</li>
  <li>このchatを利用された場合、その時点でこの注意事項を了承されたものとみなします。</li>
  <li>この chat の発言記録は <a href=http://suika.suikawiki.org/c/gpl target=_top>GPL</a> として扱うものとします。</li>
  <li>この注意事項は予告無く変更される場合があります。</li>
  <li>その他疑問点等がある場合は、その判断は管理人に委ねられるものとします。管理人に御問い合わせ下さい。</li>
</ul>

<h2>HTML tagの使用</h2>

<p>HTML の要素の開始・終了を表すタグや、名前実体参照,
数値文字参照, マーク区間などは使用出来ません。</p>
EOM
  $s;
}


          ### その他
          ### メッセイジの色
          $defcolor = 'gray';                   # defaultの色

## w_chat_log_clear - $backlogfile(「もっと前」の記録)が大きくなったら、
##                    自動的にメイルを送信し、削除する。
##
sub chat::log::clear {
 open (DB,$backlogfile) || &SITE::error("$backlogfileを開けませんでした。");
  @w_log_file = <DB>;
 close(DB);
 
 local($date_now) = &SITE::get_time('s');
 
 open (DB,"> $backlogfile") || &SITE::error("$backlogfileを開けませんでした。");
  print DB "$date_now<>$chat::member::master::pointer<strong>$chat::member::master::name</strong><><>発言の記録は一旦削除されました。記録は、$chat::admin::nameが保管しています。<>$chat::member::master::color<><>";
 close(DB);
 
 ## rankingに追加
 &chat::rank::add($chat::member::master::name);

}

## -- Sending report mail
package chat::mail;
use Chat::Util;

sub login {
  local($visitor,$vmail) = @_;
  local($addr) = $main::ENV{'REMOTE_ADDR'} || $main::ENV{'REMOTE_HOST'};
  #local($host) = gethostbyaddr(pack('C4',split(/\./,$host)),2) || $addr;
  local($mlid) = &SITE::count($chat::id.'::login');
  $visitor = '<a href="mailto:'.(htescape $vmail).'">'.(htescape $visitor).'</a>';
}


sub chat::longlog::expire (%) {
  my %o = @_;
  open(DB, $chat::longlog::filename)
     || &SITE::error($chat::longlog::filename.$chat::error::open);
    my @longlog = <DB>;
  close(DB);
  return if ($#longlog < $chat::longlog::expire && !$o{force});
}

sub chat::member::ranking::report {
}

sub chat::munou::sendbrain {
}


package chat::member::ranking;

sub rank2html {
  local(@rank,$s,$rank,%R,%count,$rank1,$rank2,$count_tmp);
  open(IN, $filename) || &SITE::error($filename.$chat::error::open);
    @rank = <IN>;
  close(IN);
  
  $s .= <<EOH;
<h2>$name</h2>
<table id="ranking">
<thead>
<tr><th class="level">順位<th class="name">名前<th class="count">回数
<tbody>
EOH
  foreach $rank (@rank) {
    ($R{name}, $R{count}, $R{date}) = split(/<>/,$rank);
    $count{$R{name}} = $R{count};
  }
  
  $rank1 = 0;    $rank2 = 1;
  $count_tmp = 0;
  foreach (sort { ($count{$b} <=> $count{$a}) || ($a cmp $b)} keys(%count)) {
    ($count{$_} == $count_tmp) || ($rank1 = $rank2);
    $s .= "<tr><td class=\"level\">第${rank1}位<td class=\"name\">$_<td class=\"count\">${count{$_}}\n";
    $count_tmp = $count{$_};
    $rank2++;
  }
  $s .= "</table>\n";
  $s .= "<p class=\"note\">(ここ${limit}日の発言数)\n";
  $s .= $SITE::about_html;
  $s;
}

sub chat::uriencode ($) {
  my $s = shift;
    $s = Jcode->new ($s, 'euc')
                  ->tr ("\xA3\xB0-\xA3\xB9\xA3\xC1-\xA3\xDA\xA3\xE1-\xA3\xFA\xA1\xF5\xA1\xA4\xA1\xA5\xA1\xA7\xA1\xA8\xA1\xA9\xA1\xAA\xA1\xAE\xA1\xB0\xA1\xB2\xA1\xBF\xA1\xC3\xA1\xCA\xA1\xCB\xA1\xCE\xA1\xCF\xA1\xD0\xA1\xD1\xA1\xDC\xA1\xF0\xA1\xF3\xA1\xF4\xA1\xF6\xA1\xF7\xA1\xE1\xA2\xAF\xA2\xB0\xA2\xB2\xA2\xB1\xA1\xE4\xA1\xE3\xA1\xC0\xA1\xA1" => q(0-9A-Za-z&,.:;?!`^_/|()[]{}+$%#*@='"~-><\ ))
                  ->jis;
  $s =~ s/([^A-Za-z0-9_@.-])/sprintf("%%%02X", ord($1))/eg;
  $s;
}

=head1 AUTHOR

Wakaba <wakaba@suikawiki.org>.

=head1 LICENSE

Copyright 2000-2022 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
