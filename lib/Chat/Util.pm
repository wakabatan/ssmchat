package Chat::Util;
use strict;
use warnings;
our $VERSION = '1.0';

use Exporter::Lite;
use Time::Local qw(timegm_nocheck);

our @EXPORT = qw(htescape time2httime);

sub htescape ($) {
  my $s = shift;
  $s =~ s/&/&amp;/g;
  $s =~ s/</&lt;/g;
  $s =~ s/>/&gt;/g;
  $s =~ s/\x22/&quot;/g;
  return $s;
} # htescape

my $mon2m = {
  jan => 1, feb => 2, mar => 3, apr => 4, may => 5, jun => 6,
  jul => 7, aug => 8, sep => 9, oct => 10, nov => 11, dec => 12,
};

sub time2httime ($) {
  my $s = shift;
  if ($s =~ /^([0-9]+) ([A-Za-z]+) ([0-9]+) ([0-9]+):([0-9]+):([0-9]+) ([+-][0-9]{2})([0-9]+)$/) {
    my $m = $mon2m->{lc $2};
    return sprintf '<time datetime="%04d-%02d-%02dT%02d:%02d:%02d%s:%s">%s</time>',
        $3, $m, $1, $4, $5, $6, $7, $8, htescape $s;
  } else {
    return htescape $s;
  }
} # time2httime

1;

=head1 NAME

Chat::Util - ssmchat Utilities

=head1 AUTHOR

Wakaba <w@suika.fam.cx>.

=head1 LICENSE

Copyright 2010 Wakaba <w@suika.fam.cx>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
